package com.eximiomedia

class AuthenticationToken {
    
    String username
    String token

    static constraints = {
    }
}
