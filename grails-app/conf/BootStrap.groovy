import com.eximiomedia.User
import com.eximiomedia.Authority
import com.eximiomedia.UserAuthority

class BootStrap {

    def init = { servletContext ->
        def roleAdmin = Authority.findByAuthority('ROLE_ADMIN')?:new Authority(authority:'ROLE_ADMIN').save(flush:true)
        def roleDoc = Authority.findByAuthority('ROLE_DOCTOR')?:new Authority(authority:'ROLE_DOCTOR').save(flush:true)
        def roleUser = Authority.findByAuthority('ROLE_USER')?:new Authority(authority:'ROLE_USER').save(flush:true)
		
        def userAdmin = User.findByUsername('admin@dokterdash.com')?:new User(username:'admin@dokterdash.com',password:'123456',fullname:'Doctor Dashboard Admin').save(flush:true)
		
        if (!userAdmin.authorities.contains(roleAdmin)) {
            UserAuthority.create userAdmin, roleAdmin
        }
        
        def userDoc = User.findByUsername('adhiatma@meetdoctor.com')?:new User(username:'adhiatma@meetdoctor.com',password:'123456',fullname:'Dr. Adhiatma Gunawan').save(flush:true)
		
        if (!userDoc.authorities.contains(roleDoc)) {
            UserAuthority.create userDoc, roleDoc
        }
        
        def user = User.findByUsername('didinj@gmail.com')?:new User(username:'didinj@gmail.com',password:'123456',fullname:'Didin Jamaludin').save(flush:true)
		
        if (!user.authorities.contains(roleUser)) {
            UserAuthority.create user, roleUser
        }
    }
    def destroy = {
    }
}
