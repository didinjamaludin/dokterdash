environments {
    development {        
        grails {
            mongo {
                host = "localhost"
                port = 27017
                username = ""
                password = ""
                databaseName = "dokterdash"
            }
        }
    }
    production {
        grails {
            mongo {
                host = "mongodb://$OPENSHIFT_MONGODB_DB_HOST/"
                port = $OPENSHIFT_MONGODB_DB_PORT
                username = "admin"
                password = "31jm2Qtp1gAi"
                databaseName = "dokterdash"
            }
        }
    }
}
