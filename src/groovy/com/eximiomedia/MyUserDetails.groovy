/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.eximiomedia

/**
 *
 * @author DIDIN
 */
import grails.plugin.springsecurity.userdetails.GrailsUser

import org.springframework.security.core.GrantedAuthority 
import org.springframework.security.core.userdetails.User

class MyUserDetails extends GrailsUser {
    final String fullname

    MyUserDetails(String username, String password, boolean enabled,
        boolean accountNonExpired, boolean credentialsNonExpired,
        boolean accountNonLocked,
        Collection<GrantedAuthority> authorities,
        long id, String fullname) {
        
        super(username, password, enabled, accountNonExpired,
            credentialsNonExpired, accountNonLocked, authorities, id)
        
        this.fullname = fullname
    }
}